# ignore

`parse .gitignore in go`

解析 gitignore 格式文件然后进行匹配

_**usage:**_

```go
import (
    "log"
    "strings"
    "gitee.com/go-wena/ignore"
)

func main(){
    //matcher, err := ignore.FromFile(".gitignore", ".")
    //if err != nil {
    //	log.Fatal(err)
    //}
    matcher := ignore.New(strings.NewReader("*.exe\n*.out\nbin/"), ".")
    log.Printf("./bin: %t\n", matcher.Match("./bin", true))
    log.Printf("./bin/file: %t\n", matcher.Match("./bin/file", false))
    log.Printf("./file.exe: %t\n", matcher.Match("./file.exe", false))
    log.Printf("./some: %t\n", matcher.Match("./some", false))
}
```
